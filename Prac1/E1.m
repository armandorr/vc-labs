clear
close all
% Point cloud creation
x = rand(1,100) + rand();   % 100 punts aleatoris amb un offset també aleatori
y = rand().*x  + rand(1,100)/10; % pendent de valor aleatori i offset també aleatori

figure
scatter(x,y);
axis('equal')

% Cloud point centering
xp = x - mean(x);
yp = y - mean(y);

% Covariance and eigen values
c = cov(xp, yp);
[evectors, evalues] = eig(c);

% Determine which dimension has the major variance
[val,ind] = max(diag(evalues));

theta = -pi/2-atan2(evectors(ind,1),evectors(ind,2));

R = [cos(theta) sin(theta); -sin(theta) cos(theta)]; 

% Rotate the points 
rp = R * [xp;yp];

% Draw thepoints
figure
scatter(rp(1,:),rp(2,:));
axis('equal')
