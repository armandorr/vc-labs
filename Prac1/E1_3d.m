% Point cloud creation
clear
close all
x = rand(1,100) + rand();
y = rand().*x  + rand(1,100)/10;
z = rand().*x + rand().*y + rand(1,100)/10;

figure
scatter3(x,y,z);
xlabel("x")
ylabel("y")
zlabel("z")
axis('equal')

% Cloud point centering
xp = x - mean(x);
yp = y - mean(y);
zp = z - mean(z);

% Covariance and eigen values
A = [xp ;yp; zp];
c = cov(A');
[evectors, evalues] = eig(c);

% Determine which dimension has the major variance
[val,ind] = max(diag(evalues));

vector = evectors(ind,:);

alpha = cos(vector(2)/sqrt(vector(2)^2+vector(3)^2));
beta = cos(vector(1)/sqrt(vector(1)^2+vector(3)^2));
gamma = cos(vector(1)/sqrt(vector(1)^2+vector(2)^2));

Rx = [1 0 0; 0 cos(alpha) -sin(alpha); 0 sin(alpha) cos(alpha)];
Ry = [cos(beta) 0 sin(beta); 0 1 0; -sin(beta) 0 cos(beta)];
Rz = [cos(gamma) -sin(gamma) 0; sin(gamma) cos(gamma) 0; 0 0 1];

R = Rz*Ry*Rx;

% Rotate the points
rp = R * [xp;yp;zp];

% Draw the points
figure
scatter3(rp(1,:),rp(2,:),rp(3,:));
xlabel("x")
ylabel("y")
zlabel("z")
axis('equal')
