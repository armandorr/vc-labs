close all
clear
clc

I = double(imread('Wally.png'))/255;
IG = rgb2gray(I);
R = medfilt2(I(:,:,1),[1 4]);
G = medfilt2(I(:,:,2),[1 4]);
B = medfilt2(I(:,:,3),[1 4]);
RGB = cat(3,R,G,B);

HSV = rgb2hsv(RGB);
H = HSV(:,:,1);

Ytol = 10/360;
Yangle = 60/360;
Y = abs(H-Yangle) < Ytol;

Ktol = 0.20;
K = R < Ktol & G < Ktol & B < Ktol;

LH3 = strel('line',3,0);
LV7 = strel('line',7,90);
R152 = strel('rectangle',[15 2]);

Y = imerode(Y,LH3);
Y = imclose(Y,LV7);

K = imerode(K,LH3);
K = imclose(K,R152);
K = imerode(K,LV7);

RES = Y&K;

RES = imdilate(RES,strel('disk',7));

Z = zeros(size(RES));
RES = cat(3,RES*255,Z,Z);

RES = imfuse(RES, IG, 'blend');

imshow(RES)
