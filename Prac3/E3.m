close all
clear
clc

A = rgb2gray(double(imread('_61A5845.JPG'))/255);
B = rgb2gray(double(imread('_61A5855.JPG'))/255);
C = rgb2gray(double(imread('_61A5861.JPG'))/255);

mAg = gaussianFunction(A)
mBg = gaussianFunction(B)
mCg = gaussianFunction(C)

mAd = centerDistFunction(A)
mBd = centerDistFunction(B)
mCd = centerDistFunction(C)

function imgOut = sobelFilter(imgIn)
    hv = [-1 -2 -1; 0 0 0; 1 2 1];
    hh = hv';
    imgInV = imfilter(imgIn,hv);
    imgInH = imfilter(imgIn,hh);
    imgOut = abs(imgInV)+abs(imgInH);
end

function m = gaussianFunction(imgIn)
    imgInS = sobelFilter(imgIn);

    [r,c] = size(imgInS);
    g = 1000*fspecial('gaussian',[r,c],(r+c)/20);
    imgOut = imgInS.*g;
    m = sum(imgOut,'all');

    plotImage3d(imgOut)
end

function m = centerDistFunction(imgIn)
    imgInS = sobelFilter(imgIn);
    [r,c] = size(imgInS);
    p1 = r/2;
    p2 = 0;
    if r < c
        p1 = 0;
        p2 = c/2;
    end
    maxDist = (power(p1-r/2,2)+power(p2-c/2,2))/2;
    for row = 1:r
        for col = 1:c
            d = power(row-r/2,2)+power(col-c/2,2);
            t = d/maxDist;
            if(t < 1)
                imgInS(row,col) = imgInS(row,col)*(1-t);
            else
                imgInS(row,col) = 0;
            end
        end
    end
    m = sum(imgInS,'all')/100;

    plotImage3d(imgInS)
end

function plotImage3d(imgIn)
    [r,c] = size(imgIn);
    figure
    surf([1:c],[1:r],imgIn);
    shading interp;
end
